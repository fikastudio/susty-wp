<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'susty-wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~DV,Gv2yS*z:L%<5@a`AU9>m7^X9vgs_6c7,zH*A/sB>Hu~=(WpaB<QOmIKwid#(' );
define( 'SECURE_AUTH_KEY',  ')fG+b}vXD$a{u|zNGHhK;0FlR>AnD0,A:(hTtrH7>z[$6F_O79`..EW{/+92r$lq' );
define( 'LOGGED_IN_KEY',    'S|94g 2m8,Okz~vR&r7#S{*aEFT=1|5e2yHH?H[h=0uipqgwJua)g#4SSmV)*Yz|' );
define( 'NONCE_KEY',        'Nsd;VQ,,fLQ%u^v*86hFqy=P!hXN@OhVWajzc3i35|/k{7EGwI+ia2qA5ugsMcJj' );
define( 'AUTH_SALT',        'ixH$V|6u?E@EIgKx4R*Ow-l;vc+xv)3FCi=b%#l9s_Wh$=yo^!%-n`pgF }t],1=' );
define( 'SECURE_AUTH_SALT', 'NewCeh)aq6DxKoafGnRVs+qwRP>jp6;un{w]AvZ=KRa6ynI.2LWwob)UO152Pif.' );
define( 'LOGGED_IN_SALT',   '{#9i9,)knoau-sKqRXX74Pg*b:-Mml4PGN[YU=8>M:8HB[Q0Odpxz5E`Y,[Z#`6t' );
define( 'NONCE_SALT',       '0W~Nfp8Q3_AcUIhcY*~x>Haa!x1RCM<[x?bNN7F_X~J+:>G;3uI!~8brCC~<]ZIz' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
